from flask import (
    Flask,
    render_template,
    Markup,
    url_for,
    flash,
    redirect,
    request
)

from datetime import date
from flask_sqlalchemy import SQLAlchemy
from flask_migrate import Migrate
from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, SubmitField
from wtforms.validators import ValidationError, DataRequired, Email, EqualTo
from wtforms import TextAreaField
from wtforms.validators import DataRequired
from wtforms.validators import Length
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager
from flask_login import UserMixin
from flask_login import current_user, login_user
from flask_login import logout_user
from flask_login import login_required
from flask_bootstrap import Bootstrap
from datetime import datetime

import os
basedir = os.path.abspath(os.path.dirname(__file__))



# Setup
app = Flask(__name__)
app.config["SECRET_KEY"] = "grafanaissick"
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ.get('DATABASE_URL') or \
     'sqlite:///' + 'C:\\...path...\\app.db'
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db = SQLAlchemy(app)
migrate = Migrate(app, db)
login = LoginManager(app)
bootstrap = Bootstrap(app)


# Variables

products_info = [
    {
        "id": "101",
        "name": "NELL",
        "img": "laptop-101.jpg",
        "price": 18,
        "paypal": "LNRBY7XSXS5PA",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "102",
        "name": "Master Control Program",
        "img": "laptop-102.jpg",
        "price": 20,
        "paypal": "XP8KRXHEXMQ4J",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "103",
        "name": "ROK",
        "img": "laptop-103.jpg",
        "price": 20,
        "paypal": "95C659J3VZGNJ",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "104",
        "name": "Dr. Know",
        "img": "laptop-104.jpg",
        "price": 18,
        "paypal": "Z5EY4SJN64SLU",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "105",
        "name": "Synapse",
        "img": "laptop-105.jpg",
        "price": 25,
        "paypal": "RYAGP5EWG4V4G",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "106",
        "name": "Red Queen",
        "img": "laptop-106.jpg",
        "price": 20,
        "paypal": "QYHDD4N4SMUKN",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "107",
        "name": "VIKI",
        "img": "laptop-107.jpg",
        "price": 20,
        "paypal": "RSDD7RPZFPQTQ",
        "colour": ["black", "white", "gray"]
    },

    {
        "id": "108",
        "name": "TARDIS",
        "img": "laptop-108.jpg",
        "price": 25,
        "paypal": "LFRHBPYZKHV4Y",
        "colour": ["black", "white", "gray"]
    }
]





# Models

subscribers = db.Table('subscribers',
    db.Column('subscriber_id', db.Integer, db.ForeignKey('user.id')),
    db.Column('subscribed_id', db.Integer, db.ForeignKey('user.id'))
)        


class LoginForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    password = PasswordField('Password', validators=[DataRequired()])
    remember_me = BooleanField('Remember Me')
    submit = SubmitField('Sign In')

    
class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    username = db.Column(db.String(64), index=True, unique=True)
    email = db.Column(db.String(120), index=True, unique=True)
    password_hash = db.Column(db.String(128))
    reviews = db.relationship('Review', backref='author', lazy='dynamic')
    subscribed = db.relationship(
        'User', secondary=subscribers,
        primaryjoin=(subscribers.c.subscriber_id == id),
        secondaryjoin=(subscribers.c.subscribed_id == id),
        backref=db.backref('subscribers', lazy='dynamic'), lazy='dynamic')

    def set_password(self, password):
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        return check_password_hash(self.password_hash, password)

    def subscribe(self, user):
        if not self.is_subscribing(user):
            self.subscribed.append(user)

    def unsubscribe(self, user):
        if self.is_subscribing(user):
            self.subscribed.remove(user)

    def is_subscribing(self, user):
        return self.subscribed.filter(
            subscribers.c.subscribed_id == user.id).count() > 0        

    def subscribed_reviews(self):
        return Review.query.join(
            subscribers, (subscribers.c.subscribed_id == Review.user_id)).filter(
                subscribers.c.subscriber_id == self.id).order_by(
                    Review.timestamp.desc())
    def __repr__(self):
        return '<User {}>'.format(self.username)    

    def subscribed_reviews(self):
        subscribed = Review.query.join(
            subscribers, (subscribers.c.subscribed_id == Review.user_id)).filter(
                subscribers.c.subscriber_id == self.id)
        own = Review.query.filter_by(user_id=self.id)
        return subscribed.union(own).order_by(Review.timestamp.desc())    

class Review(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    body = db.Column(db.String(140))
    timestamp = db.Column(db.DateTime, index=True, default=datetime.utcnow)
    user_id = db.Column(db.Integer, db.ForeignKey('user.id'))

    def __repr__(self):
        return '<Review {}>'.format(self.body)    

class ReviewForm(FlaskForm):
    review = TextAreaField('Say something', validators=[
        DataRequired(), Length(min=1, max=140)])
    submit = SubmitField('Submit')            


def get_list_view_html(product):
    """Function to return html for given laptop

    New entries should subscribe this structure:
    {
        "id": "machine_id",
        "name": "name_of_machine",
        "img": "image_name.jpg",
        "price": price_of_laptop_as_int_or_flat,
        "paypal": "paypal_id"
        "colour": ["array_of_colour"]
    }
    """
    output = ""
    image_url = url_for("static", filename=product["img"])
    laptop_url = url_for("laptop", product_id=product["id"])
    output = output + "<li>"
    output = output + '<a href="' + laptop_url + '">'
    output = (
        output + '<img src="' + image_url +
        '" al  t="' + product["name"] + '">')
    output = output + "<p>View Details</p>"
    output = output + "</a>"
    output = output + "</li>"

    return output



"""Password hashing """

def set_password(self, password):
        self.password_hash = generate_password_hash(password)

def check_password(self, password):
        return check_password_hash(self.password_hash, password)   


""" A user loader function, that can be called to load a user given the ID."""

@login.user_loader
def load_user(id):
    return User.query.get(int(id))        

"""Registration function"""

class RegistrationForm(FlaskForm):
    username = StringField('Username', validators=[DataRequired()])
    email = StringField('Email', validators=[DataRequired(), Email()])
    password = PasswordField('Password', validators=[DataRequired()])
    password2 = PasswordField(
        'Repeat Password', validators=[DataRequired(), EqualTo('password')])
    submit = SubmitField('Register')

    def validate_username(self, username):
        user = User.query.filter_by(username=username.data).first()
        if user is not None:
            raise ValidationError('Please use a different username.')

    def validate_email(self, email):
        user = User.query.filter_by(email=email.data).first()
        if user is not None:
            raise ValidationError('Please use a different email address.')




# Routes

@app.route("/")
def index():

    context = {"page_title": "BOI", "current_year": date.today().year}
    counter = 0
    product_data = []
    for product in products_info:
        counter += 1
        if counter < 5:  # First 4 laptops
            product_data.append(
                Markup(get_list_view_html(product))
            )
    context["product_data"] = Markup("".join(product_data))
    return render_template("index.html", **context)


@app.route("/laptops")
def laptops():
    # Listing page func
    context = {"page_title": "BOI", "current_year": date.today().year}
    product_data = []
    for product in products_info:
        product_data.append(Markup(get_list_view_html(product)))
    context["product_data"] = Markup("".join(product_data))
    return render_template("laptops.html", **context)


@app.route("/laptop/<product_id>")
def laptop(product_id):
    """Function for Individual Laptop Page"""
    context = {"page_title": "BOI", "current_year": date.today().year}
    my_product = ""
    for product in products_info:
        if product["id"] == product_id:
            my_product = product
    context["product"] = my_product
    return render_template("laptop.html", **context)

@app.route('/login', methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = LoginForm()
    if form.validate_on_submit():
        user = User.query.filter_by(username=form.username.data).first()
        if user is None or not user.check_password(form.password.data):
            flash('Invalid username or password')
            return redirect(url_for('login'))
        login_user(user, remember=form.remember_me.data)
        return redirect(url_for('index'))
    return render_template('login.html', title='Sign In', form=form)


@app.route('/logout')
def logout():
    logout_user()
    return redirect(url_for('index'))


@app.route('/register', methods=['GET', 'POST'])
def register():
    if current_user.is_authenticated:
        return redirect(url_for('index'))
    form = RegistrationForm()
    if form.validate_on_submit():
        user = User(username=form.username.data, email=form.email.data)
        user.set_password(form.password.data)
        db.session.add(user)
        db.session.commit()
        flash('Congratulations, you are now a registered user!')
        return redirect(url_for('login'))
    return render_template('register.html', title='Register', form=form)



@app.route("/receipt")
def receipt():
    """Function to display receipt after purchase"""
    context = {"page_title": "BOI", "current_year": date.today().year}
    return render_template("receipt.html", **context)


@app.route('/user/<username>', methods=['GET','POST'])
@login_required
def user(username):
    user = User.query.filter_by(username=username).first_or_404()
    form = ReviewForm()
    if form.validate_on_submit():
        review = Review(body=form.review.data, author=current_user)
        db.session.add(review)
        db.session.commit()
        flash('Your review is now live!')
        return redirect(url_for('index'))
    reviews = current_user.subscribed_reviews().all()

    return render_template('user.html', user=user, reviews=reviews, form=form)    


@app.route('/subscribe/<username>')
@login_required
def subscribe(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot subscribe to yourself!')
        return redirect(url_for('user', username=username))
    current_user.subscribe(user)
    db.session.commit()
    flash('You are subscribing to {}!'.format(username))
    return redirect(url_for('user', username=username))

@app.route('/unsubscribe/<username>')
@login_required
def unsubscribe(username):
    user = User.query.filter_by(username=username).first()
    if user is None:
        flash('User {} not found.'.format(username))
        return redirect(url_for('index'))
    if user == current_user:
        flash('You cannot unsubscribe yourself!')
        return redirect(url_for('user', username=username))
    current_user.unsubscribe(user)
    db.session.commit()
    flash('You are not subscribing to {}.'.format(username))
    return redirect(url_for('user', username=username))



# Run application
if __name__ == "__main__":
    app.run(debug=True)
