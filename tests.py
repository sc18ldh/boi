from datetime import datetime, timedelta
import unittest
from app import app, db
from app import User, Review

class UserModelCase(unittest.TestCase):
    def init(self):
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite://'
        db.create_all()

    def destroy(self):
        db.session.remove()
        db.drop_all()

    def test_hash(self):
        u = User(username='thetoddboi')
        u.set_password('letschessit')
        self.assertFalse(u.check_password('nope'))
        self.assertTrue(u.check_password('letschessit'))

    def test_subscribe(self):
        u1 = User(username='luke', email='luke@testcase.com')
        u2 = User(username='notluke', email='notluke@testcase.com')
        db.session.add(u1)
        db.session.add(u2)
        db.session.commit()
        self.assertEqual(u1.subscribed.all(), [])
        self.assertEqual(u1.subscribers.all(), [])

        u1.subscribe(u2)
        db.session.commit()
        self.assertTrue(u1.is_subscribed(u2))
        self.assertEqual(u1.subscribed.count(), 1)
        self.assertEqual(u1.subscribed.first().username, 'notluke')
        self.assertEqual(u2.subscribers.count(), 1)
        self.assertEqual(u2.subscribers.first().username, 'luke')

        u1.unsubscribe(u2)
        db.session.commit()
        self.assertFalse(u1.is_subscribed(u2))
        self.assertEqual(u1.subscribed.count(), 0)
        self.assertEqual(u2.subscribers.count(), 0)

    def test_subscribe_reviews(self):
        # create four users
        u1 = User(username='luke', email='luke@testcase.com')
        u2 = User(username='notluke', email='notluke@testcase.com')
        u3 = User(username='yourboi', email='yourboi@testcase.com')
        u4 = User(username='notyourboi', email='notyourboi@testcase.com')
        db.session.add_all([u1, u2, u3, u4])

        # create four reviews
        now = datetime.utcnow()
        p1 = Review(body="Review from luke", author=u1,
                  timestamp=now + timedelta(seconds=1))
        p2 = Review(body="Review from notluke", author=u2,
                  timestamp=now + timedelta(seconds=4))
        p3 = Review(body="Review from yourboi", author=u3,
                  timestamp=now + timedelta(seconds=3))
        p4 = Review(body="Review from notyourboi", author=u4,
                  timestamp=now + timedelta(seconds=2))
        db.session.add_all([p1, p2, p3, p4])
        db.session.commit()

        # setup the subscribers
        u1.subscribe(u2)  # luke subscribes to notluke
        u1.subscribe(u4)  # luke subscribes to notyourboi
        u2.subscribe(u3)  # notluke subscribes to yourboi
        u3.subscribe(u4)  # yourboi subscribes to notyourboi
        db.session.commit()

        # check the subscribed reviews of each user
        f1 = u1.subscribed_reviews().all()
        f2 = u2.subscribed_reviews().all()
        f3 = u3.subscribed_reviews().all()
        f4 = u4.subscribed_reviews().all()
        self.assertEqual(f1, [p2, p4, p1])
        self.assertEqual(f2, [p2, p3])
        self.assertEqual(f3, [p3, p4])
        self.assertEqual(f4, [p4])

if __name__ == '__main__':
    unittest.main(verbosity=2)